# python-r3

Python wrapper for [R3](https://github.com/c9s/r3) router.

Design:
- low level API following the C one, but making it pythonic (hide `match_entry`
  and `strlen(path)` for example, strip out the `r3_` prefix…)
- unit tests
- pip installable
- framework agnostic


## Install

    pip install https://framagit.org/drone/python-r3

## API

```python
from r3.tree import Tree
tree = Tree()
# `data` can be anything
tree.insert_route(path=b'/bar', method=tree.GET, data={'ah': 'ok'})
tree.insert_route(path=b'/zoo', method=tree.GET, data='anything')
tree.insert_route(path=b'/foo/bar', method=tree.GET, data=[1, 2, 3])
tree.compile()
tree.match_route(path=b'/foo/bar', method=tree.GET)
```

## source build

    make configure
    make compile


## Tests

    make test


## Inspiration/alternatives

- https://github.com/lucemia/pyr3 (original POC)
- https://github.com/thedrow/pyr3 (cffi based)
- https://github.com/iceb0y/aiohttp-r3 (tied to aiohttp)
