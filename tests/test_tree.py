import pytest

from r3.tree import Tree


@pytest.fixture
def tree():
    tree_ = Tree()
    yield tree_
    del tree_


def test_simple_match(tree):
    tree.insert_path(b'/foo', data='x')
    tree.compile()
    assert tree.match(b'/foo') == ('x', [])


def test_match_return_params(tree):
    tree.insert_path(b'/foo/{id}', data='x')
    tree.compile()
    assert tree.match(b'/foo/123') == ('x', [b'123'])


def test_simple_match_route(tree):
    tree.insert_route(tree.GET, b'/foo', data='x')
    tree.compile()
    assert tree.match_route(tree.GET, b'/foo') == ('x', [])
    assert tree.match_route(tree.GET, b'/foo') == ('x', [])


def test_match_route_return_params(tree):
    tree.insert_route(tree.GET, b'/foo/{id}', data='x')
    tree.compile()
    assert tree.match_route(tree.GET, b'/foo/bar')[1] == [(b'id', b'bar')]
    assert tree.match_route(tree.GET, '/foo/bar'.encode())[1]


def test_match_route_param_regex_can_be_changed(tree):
    tree.insert_route(tree.GET, b'/foo/{id:\d+}', data='x')
    tree.compile()
    assert tree.match_route(tree.GET, b'/foo/bar') == (None, None)
    assert tree.match_route(tree.GET, b'/foo/22') == ('x', [(b'id', b'22')])


def test_match_route_param_regex_can_consume_slash(tree):
    tree.insert_route(tree.GET, b'/foo/{path:.+}', data='x')
    tree.compile()
    assert tree.match_route(tree.GET, b'/foo/path/to/somewhere') == \
        ('x', [(b'path', b'path/to/somewhere')])


def test_match_route_segment_can_mix_string_and_param(tree):
    tree.insert_route(tree.GET, b'/foo.{ext}', data='x')
    tree.compile()
    assert tree.match_route(tree.GET, b'/foo.json')[1] == [(b'ext', b'json')]
    assert tree.match_route(tree.GET, b'/foo.txt')[1] == [(b'ext', b'txt')]


def test_match_route_honour_method(tree):
    tree.insert_route(tree.GET, b'/foo/', data='x')
    tree.compile()
    assert tree.match_route(tree.POST, b'/foo/') == (None, None)
    assert tree.match_route(tree.GET, b'/foo/') == ('x', [])


def test_match_route_accept_multiple_methods(tree):
    tree.insert_route(tree.GET | tree.POST, b'/foo/', data='x')
    tree.compile()
    assert tree.match_route(tree.POST, b'/foo/') == ('x', [])
    assert tree.match_route(tree.GET, b'/foo/') == ('x', [])


def test_match_route_can_register_method_separately(tree):
    tree.insert_route(tree.GET, b'/foo/', data='x')
    tree.insert_route(tree.POST, b'/foo/', data='y')
    tree.compile()
    assert tree.match_route(tree.POST, b'/foo/') == ('y', [])
    assert tree.match_route(tree.GET, b'/foo/') == ('x', [])


@pytest.mark.xfail
def test_match_route_can_be_overriden_before_compile(tree):
    tree.insert_route(tree.GET, b'/foo/', data='old')
    tree.insert_route(tree.GET, b'/foo/', data='new')
    tree.compile()
    assert tree.match_route(tree.GET, b'/foo/') == ('new', [])


@pytest.mark.xfail
def test_match_route_can_be_overriden_after_compile(tree):
    tree.insert_route(tree.GET, b'/foo/', data='old')
    tree.compile()
    tree.insert_route(tree.GET, b'/foo/', data='new')
    tree.compile()
    assert tree.match_route(tree.GET, b'/foo/') == ('new', [])


def test_match_route_accept_dict_as_data(tree):
    tree.insert_route(tree.GET, b'/foo', data={'x': 'y'})
    tree.compile()
    assert tree.match_route(tree.GET, b'/foo') == ({'x': 'y'}, [])


def test_match_route_accept_func_as_data(tree):

    def handler():
        pass

    tree.insert_route(tree.GET, b'/foo', data=handler)
    tree.compile()
    assert tree.match_route(tree.GET, b'/foo') == (handler, [])


def test_match_route_accept_multiple_params(tree):
    tree.insert_route(tree.GET, b'/foo/{id}/bar/{sub}', data='x')
    tree.compile()
    assert tree.match_route(tree.GET, b'/foo/id/bar/sub')[1] == \
        [(b'id', b'id'), (b'sub', b'sub')]
