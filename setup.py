from pathlib import Path
from setuptools import setup, Extension

# No libjson nor graphviz support yet.
r3_src = [str(p) for p in Path('vendor/r3/src').glob('*.c')
          if not str(p).endswith(('json.c', 'gvc.c'))]

setup(
    name='python-r3',
    version='0.0.1',
    description='Python wrapper for R3 C router.',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3',
        'Operating System :: POSIX',
        'Operating System :: MacOS :: MacOS X',
        'Environment :: Web Environment',
        'Development Status :: 4 - Beta',
    ],
    platforms=['POSIX'],
    author='Yohan Boniface',
    author_email='yohan.boniface@data.gouv.fr',
    license='MIT',
    packages=['r3'],
    ext_modules=[
        Extension(
            'r3.tree',
            ['r3/tree.c', './vendor/r3/3rdparty/zmalloc.c'] + r3_src,
            extra_compile_args=['-O2'],
            libraries=["pcre"],
            include_dirs=['./vendor/r3/include', './vendor/r3', './vendor/r3/3rdparty'],
        )
    ],
    provides=['r3'],
    include_package_data=True
)
