configure:
	git submodule update --init
	cd vendor/r3 && ./autogen.sh && ./configure

compile:
	cython r3/tree.pyx
	python setup.py build_ext --inplace

test:
	py.test -v
