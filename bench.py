from timeit import timeit

from r3.tree import Tree


tree = Tree()
paths = [
    b'/user/', b'/user/{id}', b'/user/{id}/subpath', b'/user/{id}/subpath',
    b'/boat/', b'/boat/{id}', b'/boat/{id}/subpath', b'/boat/{id}/subpath',
    b'/horse/', b'/horse/{id}', b'/horse/{id}/subpath', b'/horse/{id}/subpath',
    b'/bicycle/', b'/bicycle/{id}', b'/bicycle/{id}/subpath',
    b'/bicycle/{id}/subpath']

for path in paths:
    tree.insert_route(path=path, method=tree.GET, data={'ah': 'ok'})
tree.compile()

data, params = tree.match_route(tree.GET, b'/horse/22/subpath')
print(data, params)
total = timeit("tree.match_route(tree.GET, b'/user/')", number=100000,
               globals=globals())
print(f'First flat route: \n> {total}')
total = timeit("tree.match_route(tree.GET, b'/horse/22/subpath')",
               number=100000, globals=globals())
print(f'Middle route with placeholder\n> {total}')
total = timeit("tree.match_route(tree.GET, b'/plane/')", number=100000,
               globals=globals())
print(f'Unknown route\n> {total}')
del tree
