# cython: language_level=3

cimport ctree
from libc.stdlib cimport free


cdef inline bytes read_iovec(ctree.r3_iovec_t* iovec):
    return iovec.base[:iovec.len]


cdef class Tree:

    GET = ctree.METHOD_GET
    POST = ctree.METHOD_POST
    PUT = ctree.METHOD_PUT
    DELETE = ctree.METHOD_DELETE
    PATCH = ctree.METHOD_PATCH
    HEAD = ctree.METHOD_HEAD
    OPTIONS = ctree.METHOD_OPTIONS

    cdef ctree.R3Node* root

    def __cinit__(self, int cap=10):
        self.root = ctree.r3_tree_create(cap)

    def insert_route(self, int method, bytes path, data):
        ctree.r3_tree_insert_route(self.root, method, path, <void*>data)

    def insert_path(self, bytes path, data):
        ctree.r3_tree_insert_path(self.root, path, <void*>data)

    def compile(self):
        cdef char *errstr
        if ctree.r3_tree_compile(self.root, &errstr):
            try:
                raise Exception((<bytes>errstr).decode())
            finally:
                free(errstr)

    def match(self, bytes path):
        cdef:
            ctree.match_entry *entry = ctree.match_entry_create(path)
            ctree.R3Node *match
            list params = []
            unsigned int i, n
        try:
            match = ctree.r3_tree_match_entry(self.root, entry)
            if match:
                n = entry.vars.tokens.size
                for i in range(n):
                    params.append(read_iovec(&entry.vars.tokens.entries[i]))
                return <object>match.data, params
            return None, None
        finally:
            ctree.match_entry_free(entry)

    def match_route(self, int method, bytes path):
        cdef:
            ctree.match_entry *entry = ctree.match_entry_create(path)
            ctree.R3Route *route
            # FIXME: trying to deal with params as dict will create a collision
            # with route.data, and the latest defined will erase the other...
            list params = []
            unsigned int i, n
        try:
            entry.request_method = method
            route = ctree.r3_tree_match_route(self.root, entry)
            if route:
                n = entry.vars.tokens.size
                for i in range(n):
                    params.append((read_iovec(&entry.vars.slugs.entries[i]),
                                   read_iovec(&entry.vars.tokens.entries[i])))
                return <object>route.data, params
            return None, None
        finally:
            ctree.match_entry_free(entry)

    def __dealloc__(self):
        if self.root is not NULL:
            ctree.r3_tree_free(self.root)
