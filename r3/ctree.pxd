cdef extern from 'r3.h':
    cdef enum:
        METHOD_GET
        METHOD_POST
        METHOD_PUT
        METHOD_DELETE
        METHOD_PATCH
        METHOD_HEAD
        METHOD_OPTIONS

    ctypedef struct r3_iovec_t:
        char* base
        unsigned int len

    ctypedef struct r3_vector_t:
        r3_iovec_t* entries
        unsigned int size

    ctypedef struct str_array:
        r3_vector_t slugs
        r3_vector_t tokens

    ctypedef struct R3Node:
        void* data

    ctypedef struct R3Route:
        void* data

    ctypedef struct match_entry:
        str_array vars
        int request_method

    R3Node* r3_tree_create(int cap)
    void r3_tree_free(R3Node * tree)
    int r3_tree_compile(R3Node *n, char** errstr)

    match_entry* match_entry_create(const char* path)
    match_entry* match_entry_createl(const char* path, unsigned int path_len)
    void match_entry_free(match_entry * entry)

    R3Node* r3_tree_insert_path(R3Node *tree, const char *path, void * data)
    R3Node* r3_tree_match(const R3Node * n, const char * path, match_entry * entry)
    R3Node* r3_tree_matchl(const R3Node * n, const char * path, unsigned int path_len, match_entry * entry)
    R3Node* r3_tree_match_entry(const R3Node * n, match_entry * entry)

    R3Route* r3_tree_insert_route(R3Node* tree, int method, const char* path, void* data)
    R3Route* r3_tree_match_route(const R3Node* n, match_entry* entry)
